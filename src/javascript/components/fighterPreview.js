import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (typeof fighter !== 'undefined'){
    const imgElement = createFighterImage(fighter);
    const { name, health, attack, defense } = fighter;
    const fighterData = createElement({ tagName: 'span', className: 'fighter-preview___data' });
    fighterData.innerText = [name, 'health=' + health, 'attack=' + attack, 'defense=' + defense].join('\n');
    fighterElement.append(imgElement, fighterData);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
