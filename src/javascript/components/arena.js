import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { controls as keyControls } from '../../constants/controls.js';
import { fight, getDamage } from './fight';
import { showWinnerModal } from './modal/winner';
import { getFighterInfo } from './fighterSelector';


export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner
  document.addEventListener('keypress', keyPress);
  document.addEventListener('keyup', keyUp);
    
  fight(...selectedFighters)
    .then((winner) => {
      console.log("then -> " + winner);
      let fighter = {
        title:winner.name,
        bodyElement:createFighterImage(winner),
        onClose:()=>{}
      };

      showWinnerModal(fighter);
      return;
    })
    .catch((winner) =>{
      console.log("CATCH ERROR!!! -> " + winner);
    })
    .finally((winner) => {
      console.log("FINALY -> " + winner);
    });
}

const keysMap = new Map();

function keyPress(e) {
  let _id = e.code;
  if(!isControlKey(_id)){
    return;
  }
  if(keysMap.has(_id) && keysMap.get(_id)){
    return;
  }
  keysMap.set(_id, true);
  fightAction(_id);
}

function keyUp(e) {
  let _id = e.code;
  if(!isControlKey(_id)){
    return;
  }
  keysMap.set(_id, false);
}

function fightAction(_id) {
  if(_id == keyControls.PlayerOneAttack) {
    if(keysMap.has(keyControls.PlayerOneBlock) && keysMap.get(keyControls.PlayerOneBlock)) {
      console.log("\t PlayerOne CANT Attack in Block");
      return;
    }
    console.log("\t PlayerOneAttack");
    getHitPower('left')
    // getDamage(getFighterInfo(_id), "2");
    return;
  }
  if(_id == keyControls.PlayerTwoAttack){
    if(keysMap.has(keyControls.PlayerTwoBlock) && keysMap.get(keyControls.PlayerTwoBlock)) {
      console.log("\t PlayerTwo CANT Attack in Block");
      return;
    }
    console.log("\t PlayerTwoAttack");
    getHitPower('right')
    // fight.getDamage(getFighterInfo(_id));
    return;
  }
  if(_id == keyControls.PlayerOneBlock){
    console.log("\t PlayerOneBlock");
    return;
  }
  if(_id == keyControls.PlayerTwoBlock){
    console.log("\t PlayerTwoBlock");
    return;
  }
  console.log("\t checkCriticalHit");
  checkCriticalHit(_id);
}

function getHitPower(activeSide){
  let root = document.getElementById("root");
  let attack = root.getElementsByClassName('arena___fighter arena___' + activeSide + '-fighter');

  let figtersHTML = root.getElementsByClassName('fighter-preview___img');

  let leftFigterHTML = figtersHTML[0];
  let rightFigterHTML = figtersHTML[1];

  if(activeSide === 'left'){
    
  }


  let attackedSide = activeSide == 'left' ? 'right' : 'left';
  // let attacked = document.getElementById('arena___fighter arena___' + activeSide + '-fighter');
  let attackedIndicator = document.getElementById(attackedSide + "-fighter-indicator");
  let indicatorWidth = attackedIndicator.style.width;
  indicatorWidth = indicatorWidth === "" ? 100 : indicatorWidth.substring(0, indicatorWidth.length -1);
  console.log("width-> " + indicatorWidth);
  attackedIndicator.style.width = (indicatorWidth - 10) + '%';
}

function isControlKey(_id){
  let isControl = (arrayKeys) => {
    let isCKey = false;
    for (var prop in arrayKeys) {
      if(isCKey){
        break;
      }
      let keyId = arrayKeys[prop];
      if(Array.isArray(keyId)){
        isCKey = isControl(keyId);
        continue;
      }
      if(keyId == _id){
        isCKey = true;
      }
    }
    return isCKey;
  }
  let isControlKey = isControl(keyControls);
  return isControlKey;
}

function logMapElements(value, key, map) {
  console.log(`m[${key}] = ${value}`);
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
