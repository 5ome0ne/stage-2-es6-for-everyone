import { controls } from '../../constants/controls';
import { getFighterInfo } from './fighterSelector';

export async function fight(firstFighter, secondFighter) {

  // resolve the promise with the winner when fight is over
  return new Promise((resolve, reject) => {
    const isValid = getWinner();
    
    if (isValid) {  
      resolve(isValid);  
    } else {  
      reject(Error("Error"));  
    }  
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  
  damage = damage < 0 ? 0 : damage;
  return damage
}

export function getHitPower(fighter) {
  let criticalHitChance = 1 + Math.random();
  let attack = getAttack(fighter);

  let power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  let dodgeChance = 1 + Math.random();
  let defense = getDefense(fighter);

  let power = defense * dodgeChance;
  return power;
}

export function getCriticalHitPower(fighter) {
  var now = new Date();
  console.log("TIME-> " + now);

  let attack = getAttack(fighter);

  let power = attack * 2;
  return power;
}

function getAttack(fighter){
  let fighterInfo = getFighterInfo(fighter);
  console.log(fighterInfo);

  return 10;
}

var count = 0;
async function getWinner(){
  while(count++ <= 5){
    console.log("count = " + count + " Wait a sec")
    await timeoutPromise(1000);
  }
  return await getFighterInfo(1);
}

function timeoutPromise(interval) {
  return new Promise((resolve) => {
    setTimeout(function(){
      resolve("done");
    }, interval);
  });
};
